package routes


import (
	"gitlab.com/gordonfng/goptv"
)

type Route struct {
	Route_service_status ServiceStatus 
	Route_type int
	Route_id int
	Route_Name string
	Route_Number string
	Route_GTFS_Id string
}

type ServiceStatus struct {
	Description string
	Timestamp string
}

type GetRoutesResp struct{
	Routes []Route
	Status goptv.Status	
}

type GetRouteByIdResp struct{
	Route Route
	Status goptv.Status	
}


type RoutesRequest struct{
	RouteType string
	RouteName string
}



func GetRoutes(p goptv.GoPTV, req RoutesRequest,
	respChan chan GetRoutesResp,  errChan chan error) {
	resp := GetRoutesResp{}
	data := "/v3/routes?route_name=" + req.RouteName + "&route_types=" +
		req.RouteType
	err :=p.APICall(data,&resp)
	respChan <- resp
	errChan <- err
}

func GetRouteById(p goptv.GoPTV, routeId string,
	respChan chan GetRouteByIdResp,  errChan chan error) {
	resp := GetRouteByIdResp{}
	data := "/v3/routes/" + routeId + "/?"
	err := p.APICall(data,&resp)
	respChan <- resp
	errChan <- err
}

func GetAllRoutes(p goptv.GoPTV, route_type string,
	respChan chan GetRoutesResp,  errChan chan error){
	resp := GetRoutesResp{}
	data := "/v3/routes?route_types=" + route_type 
	err := p.APICall(data,&resp)
	respChan <- resp
	errChan <- err
}
