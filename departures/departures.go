package departures
import (
	"gitlab.com/gordonfng/goptv"
	"gitlab.com/gordonfng/goptv/routes"
	"gitlab.com/gordonfng/goptv/stops"
	"gitlab.com/gordonfng/goptv/runs"
	"time"
	"fmt"
)

type DeparturesResp struct {
	Departures []Departure
	Stops map[string]stops.StopsAlongRoute
	Routes map[string]routes.Route
	Runs map[string]runs.Run
	Directions map[string]goptv.Empty
	Disruptions map[string]goptv.Empty
	Status goptv.Status
}

type Departure struct{
	Stop_id int
	Routes_id int
	Run_id int
	Direction_id int
	Disruption_ids []int
	Scheduled_departure_utc string
	Estimated_departure_utc string
	At_platform bool
	Platform_number string
	Flags string
	Departure_seqeuence int
}


type GetDeparturesByStopReq struct{
	Route_type int
	Stop_id int
	Platform_numbers []int
	Direction_id int
	Look_Backwards bool
	GTFS bool
	Date_utc time.Time
	Max_results int
	Include_cancelled bool
	Expand []string
}


func Departures(p goptv.GoPTV,
	req GetDeparturesByStopReq ,respChan chan DeparturesResp,  errChan chan error) {
	resp := DeparturesResp{}
	data := fmt.Sprintf("/v3/departures/route_type/%d/stop/%d?",
		req.Route_type,req.Stop_id)

	for _,num := range req.Platform_numbers {
		data = data + fmt.Sprintf("platform_numbers=%d&",num)
	}
	if(req.Direction_id != -1){
		data = data + fmt.Sprintf("direction_id=%d&",req.Direction_id)
	}
	data = data + fmt.Sprintf("Look_Backwards=%t&",req.Look_Backwards)
	data = data + fmt.Sprintf("gtfs=%t&",req.GTFS)
	data = data + fmt.Sprintf("date_utc=%s&",req.Date_utc.Format(time.RFC3339Nano))
	data = data + fmt.Sprintf("max_results=%d&",req.Max_results)
	data = data + fmt.Sprintf("include_cancelled=%t&",req.Include_cancelled)
	for _,num2 := range req.Expand {
		data = data + fmt.Sprintf("expand=%s&",num2)
	}	
	err :=p.APICall(data,&resp)
	respChan <- resp
	errChan <- err
}
