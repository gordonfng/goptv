package runs


import (
	"gitlab.com/gordonfng/goptv"
	"fmt"
)

type VehiclePosition struct {
	Latitude float64
	Longitude float64
	Bearing float64
	Supplier string
}

type VehicleDescription struct {
	Operator string
	Id string
	Low_floor bool
	Air_conditioned bool
	Description string
	Supplier string
}

type Run struct {
	Run_id int
	Route_id int
	Route_type int
	Final_stop_id int
	Destination_name string
	Status string
	Direction_id int
	Run_sequence int
	Express_stop_count int
	Vehicle_position VehiclePosition
	Vehicle_descriptor VehicleDescription
}

type RunsResp struct{
	Runs []Run
	Status goptv.Status
}

type RunsRequest struct{
	Route_id int
	Route_type int
}

func GetRuns(p goptv.GoPTV, req RunsRequest,
	respChan chan RunsResp,  errChan chan error) {
	resp := RunsResp{}
	data := fmt.Sprintf("/v3/runs/route/%d?", req.Route_id)
	err :=p.APICall(data,&resp)
	respChan <- resp
	errChan <- err
}

func GetRunsByType(p goptv.GoPTV, req RunsRequest,
	respChan chan RunsResp,  errChan chan error) {
	resp := RunsResp{}
	data :=  fmt.Sprintf("/v3/runs/route/%d/route_type/%d?",req.Route_id,
		req.Route_type)
	err :=p.APICall(data,&resp)
	respChan <- resp
	errChan <- err
}

func GetRunsById(p goptv.GoPTV,
	run_id int,respChan chan RunsResp,  errChan chan error) {
	resp := RunsResp{}
	data := fmt.Sprintf("/v3/runs/%d?",run_id)
	err :=p.APICall(data,&resp)
	respChan <- resp
	errChan <- err
}
