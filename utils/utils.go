package utils 
import (
	"gitlab.com/gordonfng/goptv"
)

type GetRouteTypesResp struct{
	Route_types []RouteType
	Status goptv.Status	
}

type RouteType struct{
	Route_type_name string
	Route_Type int		
}


func GetRouteTypes(p goptv.GoPTV, respChan chan GetRouteTypesResp,errChan chan error) {
	types := GetRouteTypesResp{}
	data := "/v3/route_types?"
	err := p.APICall(data,&types)
	respChan <- types
	errChan <- err
}
