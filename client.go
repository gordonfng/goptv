package goptv

import (
	"net/http"
	"log"
	"io/ioutil"
	"crypto/sha1"	
	"encoding/hex"
	"crypto/hmac"
	"encoding/json"
	"fmt"
	"errors"
)

type GoPTV struct{
	devid string
	key string
	url string 
	
}

type Status struct {
	Version string
	Health int	
}

type Message struct {
	Message string
	Status Status	
}

type Empty struct{}

func MakeClient(devid,key string) GoPTV {
	client := GoPTV{}
	client.devid = devid 
	client.key = key
	client.url = "http://timetableapi.ptv.vic.gov.au" // hardcoded because duh
	return client
}

type PTVError struct {  
	Context    string //error description
	Errno int //error number
	Err     error
}

func (e *PTVError) Error() string {  
	return fmt.Sprintf("%s (%d): %v", e.Context, e.Errno, e.Err)
}

//func (p GoPTV) APICallWrap


/*
encode_sha1() basically takes in a request alongside the devid to generated a sha1_hash
*/
func (p GoPTV) encode_sha1(request string) string {
	sig := hmac.New(sha1.New, []byte(p.key))
	sig.Write([]byte(request))
	sha1_hash := hex.EncodeToString(sig.Sum(nil))
	return sha1_hash
}

/*
Generates the URL required for an API call
*/

func (p GoPTV) formatToURL(request string) string {
	raw := request + "&devid=" + p.devid
	call := p.url + raw + "&signature=" +
		p.encode_sha1(raw)
	return call
}


/*
API Call
Input:
Request(string) : A string formatted according the PTV Timetable API guide
Output(string?) : I haven't decided what to do with the output yet. I need to figure out what to do with the output (decode from JSON)
*/
func (p GoPTV) APICall(request string, result interface{} ) ( error )  {
	url := p.formatToURL(request)
	msg := Message{}
	log.Println(url)
	
	resp, err := http.Get(url)
	if err != nil {
		return &PTVError{"DNS Resolution Failed", 1,err}
	}

	defer resp.Body.Close()
	
	body, ioerr := ioutil.ReadAll(resp.Body)
	if ioerr != nil {
		return &PTVError{"Incomplete Body", 2,ioerr}
	}

	jsonerr := json.Unmarshal(body, &result)
	if jsonerr != nil {
		return &PTVError{"JSON Parsing Error", 3,jsonerr}
	}
	jsonerr = json.Unmarshal(body, &msg)
	if jsonerr != nil {
		return &PTVError{"JSON Parsing Error", 3,jsonerr}
	} else if (msg.Message != "") {
		return &PTVError{"Request Error", 4,errors.New(msg.Message)}
	}
	return nil
}
