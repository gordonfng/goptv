package stops
import (
	"gitlab.com/gordonfng/goptv"
	"fmt"
)



type Coordinate  struct{
	Latitude float64
	Longitude float64
}


type Location struct{
	Postcode int
	Municipality string
	Municipality_id int
	Primary_stop_name string
	Road_type_primary string
	Second_stop_name string
	Road_type_Second string
	Bay_nbr int
	Suburb string
	Gps Coordinate
	
}

type Empty struct{}



type Stop struct{
	Point_Id int
	Operating_hours string
	Mode_id int
	Station_details_id int
	Flexible_stop_opening_hours string
	Stop_contact Empty
	Stop_Ticket Empty	
	Disruption_ids []int
	Station_type string
	Station_Description string
	Route_type int	
	Stop_Location Location
	Stop_amenities StopAmenities
	Stop_accessibility Empty
	Stop_Staffing Empty
	Stop_id int
	Stop_name string
}

type StopResp struct{
	Stop Stop
	Disruption Empty
	Status goptv.Status
}

type StopReq struct{
	Stop_id int
	Route_type int
	Stop_location bool
	Stop_amenities bool
}

type StopAmenities struct{
	Seat_type string
	Pay_phone bool
	Indoor_waiting_area bool
	Sheltered_waiting_area bool
	Bicycle_rack int
	Bicycle_cage bool
	Bicycle_locker int
	Luggage_locker int
	Kiosk bool
	Seat string
	Stairs string
	Baby_change_facility string
	Parkiteer bool
	Replacement_bus_stop_loc string
	QTEM bool
	Bike_storage Empty
	PID bool
	ATM Empty
	Travellers_aid bool
	Premium_stop Empty
	PSOs Empty
	Melb_bike_share Empty
	Luggage_storage Empty
	Luggage_check_in Empty
	Toilet bool
	Taxi_rank bool
	Car_parking string
	Cctv bool
}




func GetStop(p goptv.GoPTV, req StopReq,
	respChan chan StopResp,   errChan chan error) {
	resp := StopResp{}
	data := fmt.Sprintf("/v3/stops/%d/route_type/%d?",
		req.Stop_id,req.Route_type)
	data = data + fmt.Sprintf("stop_location=%t&",req.Stop_location)
	data = data + fmt.Sprintf("stop_amenities=%t&",req.Stop_amenities)
	err := p.APICall(data,&resp)
	respChan <- resp
	errChan <- err
}


type StopsAlongRoute struct{
	Disruption_ids []int
	Stop_Suburb string
	Stop_name string
	Stop_id int
	Route_type int
	Stop_latitude float64
	Stop_longitude float64
	Stop_sequence int
}


type NearbyStopsReq struct{
	Latitude float64
	Longitude float64
	Route_types []int
	Max_results int
	Max_distance float64
	Stop_disruptions bool
}

func GetNearbyStops(p goptv.GoPTV, req NearbyStopsReq ,
	respChan chan StopsAlongRouteResp, errChan chan error) {
	resp := StopsAlongRouteResp{}
	data := fmt.Sprintf("/v3/stops/location/%f,%f?",
		req.Latitude,req.Longitude)
	for _,num := range req.Route_types {
		data = data + fmt.Sprintf("route_types=%d&",num)
	}
	data = data + fmt.Sprintf("max_results=%d&",req.Max_results)
	data = data + fmt.Sprintf("max_distance=%f&",req.Max_distance)
	data = data + fmt.Sprintf("stop_disruptions=%t",req.Stop_disruptions)
	err := p.APICall(data,&resp)
	respChan <- resp
	errChan <- err
}


type StopsAlongRouteReq struct{
	Route_id int
	Route_type int
	Direction_id int
	Stop_disruptions bool
}


type StopsAlongRouteResp struct{
	Stops []StopsAlongRoute
	Disruptions Empty	
	Status goptv.Status
}


func GetStopsAlongRoute(p goptv.GoPTV, req StopsAlongRouteReq ,
	respChan chan StopsAlongRouteResp,   errChan chan error) {
	resp := StopsAlongRouteResp{}
	data := fmt.Sprintf("/v3/stops/route/%d/route_type/%d?",
		req.Route_id,req.Route_type)
	data = data + fmt.Sprintf("direction_id=%d&",req.Direction_id)
	data = data + fmt.Sprintf("stop_disruptions=%t",req.Stop_disruptions)
	err := p.APICall(data,&resp)
	respChan <- resp
	errChan <- err
}
