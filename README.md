# goptv
goptv is an API Wrapper for the Public Transport Victoria Timetable API written in Go

# Getting Started
You will need a developer key from PTV. The instructions (in RTF format) are provided [here](https://static.ptv.vic.gov.au/PTV/PTV%20docs/API/1475462320/PTV-Timetable-API-key-and-signature-document.RTF)

