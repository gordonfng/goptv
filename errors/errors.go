package errors

type PTVError struct {  
	err    string //error description
	errno int //error number

}

func (e *PTVError) Error() string {  
    return e.err
}
